// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <inttypes.h>
#include <memory>
#include "/home/halfeld/ARIEL/tmp/simplest_custom_bridge/2_ws/install/ros2_equiv_msgs/include/ros2_equiv_msgs/srv/three_ints.hpp"
#include "rclcpp/rclcpp.hpp"

using MultThreeInts = ros2_equiv_msgs::srv::ThreeInts;
rclcpp::Node::SharedPtr g_node = nullptr;

void handle_service(
  const std::shared_ptr<rmw_request_id_t> request_header,
  const std::shared_ptr<MultThreeInts::Request> request,
  const std::shared_ptr<MultThreeInts::Response> response)
{
  (void)request_header;
  RCLCPP_INFO(
    g_node->get_logger(),
    "request: %" PRId64 " * %" PRId64 " * %" PRId64, request->a, request->b, request->c);
  response->prod = request->a * request->b *request->c;
}

int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);
  g_node = rclcpp::Node::make_shared("minimal_service");
  auto server = g_node->create_service<MultThreeInts>("mult_three_ints_ROS2", handle_service);
  rclcpp::spin(g_node);
  rclcpp::shutdown();
  g_node = nullptr;
  return 0;
}
