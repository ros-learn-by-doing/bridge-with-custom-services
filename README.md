Cloning
===

`git clone --recurse-submodules git@gitlab.com:ros-learn-by-doing/bridge-with-custom-services.git`

Remember to change the branch of `ros1_bridge` to the appropriate branch for your ROS2 distro.