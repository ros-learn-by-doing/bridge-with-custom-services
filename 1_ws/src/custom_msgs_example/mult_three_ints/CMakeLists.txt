cmake_minimum_required(VERSION 3.0.2) # ToDo: Figure out the actual minimum
project(mult_three_ints)

# FIND SYSTEM PACKAGES

find_package(Boost REQUIRED COMPONENTS date_time thread)
find_package(catkin REQUIRED COMPONENTS message_generation rostime roscpp rosconsole roscpp_serialization)

# WHERE'S THE GOOD STUFF

include_directories(${catkin_INCLUDE_DIRS} /home/halfeld/ARIEL/tmp/simplest_custom_bridge/1_ws/devel/.private/three_ints_msgs/include/)
link_directories(${catkin_LIBRARY_DIRS})

# ROS MESSAGES, SERVICES, etc.

# add_service_files(DIRECTORY srv FILES ThreeInts.srv)  # Declares the services used by the package.
# generate_messages(DEPENDENCIES std_msgs)              # ToDo: Do we really depend on this?

# DEPENDENCIES

catkin_package(CATKIN_DEPENDS message_runtime std_msgs)

# MACROS

macro(rostutorial T)
  add_executable(${T} ${T}/${T}.cpp)                                  # Creates an executable for each item in passed list.
  target_link_libraries(${T} ${catkin_LIBRARIES} ${Boost_LIBRARIES})  # Links libraries.
  #add_dependencies(${T} roscpp_tutorials_gencpp)                      # Adds dependencies.
  install(TARGETS ${T}                                                # Installs.
    RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})
endmacro()

# BUILD

# Run the macro for each package
foreach(dir
    # listener
    # notify_connect
    # talker
    # babbler
    # mult_three_ints_client
    # mult_three_ints_server
    mult_three_ints_server_class
    # anonymous_listener
    # listener_with_userdata
    # listener_multiple
    # listener_threaded_spin
    # listener_async_spin
    # listener_single_message
    # listener_with_tracked_object
    # listener_unreliable
    # listener_class
    # node_handle_namespaces
    # custom_callback_processing
    # timers
    # parameters
    # cached_parameters
    )
  rostutorial(${dir})
endforeach()

# add_executable(time_api_sleep time_api/sleep/sleep.cpp)
# target_link_libraries(time_api_sleep ${catkin_LIBRARIES})
# install(TARGETS time_api_sleep
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})
 
# INSTALLATION

install(FILES
  launch/talker_listener.launch
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
)
