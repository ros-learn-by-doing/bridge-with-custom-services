#include "ros/ros.h"
#include "three_ints_msgs/ThreeInts.h"

// %Tag(CLASS_DECLARATION)%
class MultThree
{
public:
  bool mult(three_ints_msgs::ThreeInts::Request& req,
            three_ints_msgs::ThreeInts::Response& res);
};
// %EndTag(CLASS_DECLARATION)%

bool MultThree::mult(three_ints_msgs::ThreeInts::Request& req,
                     three_ints_msgs::ThreeInts::Response& res)
{
  res.prod = req.a * req.b * req.c;
  ROS_INFO("request: x=%ld, y=%ld, z=%ld", (long int)req.a, (long int)req.b, (long int)req.c);
  ROS_INFO("  sending back response: [%ld]", (long int)res.prod);
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "mult_three_ints_server");
  ros::NodeHandle n;

// %Tag(SERVICE_SERVER)%
  MultThree a;
  ros::ServiceServer ss = n.advertiseService("mult_three_ints", &MultThree::mult, &a);
// %EndTag(SERVICE_SERVER)%

  ros::spin();

  return 0;
}

